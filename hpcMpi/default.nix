
let 

  _pkgs = import <nixpkgs> { 
    config = {
      packageOverrides = pkgs: {
        pybind11 = pkgs.callPackage ../pybind11.nix { 
          inherit pkgs;
          python = pkgs.python3;
        };
      };
    };
  };

  _pyPkgs = _pkgs.python3Packages;

in 

_pyPkgs.buildPythonPackage {
  name = "hpcMpi";
  src = ./.;

  nativeBuildInputs = [
    _pkgs.pkgconfig
    _pkgs.openmpi
    _pkgs.eigen
    _pkgs.pybind11 
  ];

  buildInputs = [ 
    _pkgs.pkgconfig
    _pkgs.openmpi
    _pkgs.eigen
    _pkgs.pybind11 
 
    _pyPkgs.matplotlib
    _pyPkgs.mpi4py
    _pyPkgs.numpy
    _pyPkgs.pandas
    _pyPkgs.pkgconfig
    _pyPkgs.tkinter
  ];
}
