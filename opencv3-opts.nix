{
  enableContrib = true;
  enableCuda = false;
  enableEXR = false;
  enableFfmpeg = true;
  enableGtk3 = true;
  enableJPEG2K = false;
  enableTIFF = false;
  enableWebP = false;
}

